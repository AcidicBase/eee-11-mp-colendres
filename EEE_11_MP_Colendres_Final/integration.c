#include<math.h>
#include "integration.h"


double integ(int coe[],int size,double low,double up,double d)
{
	double answer=0;
	int i;
	
	for (; low <= up;)
	{
		for(i = 0; i < size; i++)
		{
			answer += coe[i]*pow(low,3-i)*d; //highest power is to 3, i is 0 when the algo is checking the highest power. i is 1 when checking x^2, so d is raised to 3-1 = 2; and so on... 
		}  

		low += d;
	}


	
	return answer;
}

double short_integ(int coe[], int size, int low, int up)
{
	int i;
	double answer=0;
	for(i =0; i < size; i++)
	{
		answer += ((coe[i]*pow(up,4-i))/(4-i)) - ((coe[i]*pow(low,4-i))/(4-i));

	}

	return answer;
}

