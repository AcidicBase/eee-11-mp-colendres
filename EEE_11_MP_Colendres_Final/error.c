#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include "error.h"


void check(int c, int r, int d, int argc, char *argv[])
{

		//error checking: if c, or r remain 0, return an error. If d remains zero, it's okay
		
		if (!c) error(1);
		if (!r) error(2);

		// more error checking. If the index beside argument index is not a set of parameters, or the last element in argv[], return an error
		if (c)
			{
			if (c+1 == argc) error(11);
			else if ( c < argc)
				{
					if (!(strcmp(argv[c+1], "-r")) || !(strcmp(argv[c+1], "-d"))) error(11);
				}
			}
		if (r)
		{
			if (r+1 == argc) error(21);
			else if ( r < argc)
				{
					if (!(strcmp(argv[r+1], "-c")) || !(strcmp(argv[r+1], "-d"))) error(21);
				}

			if(argv[r+1][strlen(argv[r+1])-1] == ',') error(22);
		}
		if (d)
		{
			if (d+1 == argc) error(3);
			else if ( d < argc)
				{
					if (!(strcmp(argv[d+1], "-r")) || !(strcmp(argv[d+1], "-c"))) error(3);
				}
		}
}

void error(int errorval)

{
	/*
 * 	Error types: 0 - General input error
 * 	1 - no -c argument
 * 	11 - no -c argument value
 *	12 - too many -c arguments
 * 	2 - no -r argument
 * 	21 - no -r argument value
 * 	22 - lacking 2nd r value
 * 	3 - -d was specified but no delta value
 	*/

	switch (errorval)
	{
		case 0:
		printf("Input error (Insufficient arguments, unidentified command line arguments, or repeating arguments)\n");
		exit(0);
		break;
		case 1:
		printf("-c argument required \n");
		exit(0);
		break;
		case 11:
		printf("No values for coefficients found\n");
		exit(0);
		break;
		case 12:
		printf("Too many values for -c (Maximum of four numbers)\n");
		exit(0);
		break;
		case 2:
		printf("-r argument required\n");
		exit(0);
		break;
		case 21:
		printf("No values for range found\n");
		exit(0);
		break;
		case 22:
		printf("Integral must be bounded on both sides (lack of r parameters)\n");
		printf("Please input range in this format: [LOWER_BOUND],[UPPER_BOUND]\n");
		exit(0);
		break;
		case 3:
		printf("-d was specified but no value for delta given\n");
		exit(0);
		break;
		default:
		break;
		
	}
	
}
