#include<string.h>
#include<stdlib.h>
#include "parser.h"


int parser(int loc, int size, char *argv[], int coeff_num)
{
	int i = 0;
	int counter = 0; //counts how many commas have been detected. Anything that can bound a number is considered a comma. These are the comma, and the null terminator
	int lastcomma = 0; //placeholder value for the last comma that was detected
	
	//make strings to contain substrings
	char c[1024];
	
	for (; i <=size; i++)
	{
		if(argv[loc][i] == ',' || argv[loc][i] == '\0')
		{

			counter++;
			if(counter == coeff_num)
			{
				strncpy(c, argv[loc]+lastcomma, i-lastcomma);
				c[i-lastcomma] = '\0'; 
					/*to make sure the string ends where it's supposed to end. Since string is being reused, 
					there may be instances of having a previous, longer number be partially overwritten by 
					the current number, causing the wrong number to be converted*/
				return atof(c);
			}
			lastcomma = i+1; 
				/*this is the pointer to the character after the comma. We don't want our next string to begin with a
				 comma, so this ensures that the next string starts with the first digit of the number */
				
		}
	
	}

	if(coeff_num == -1) return counter-1;//since it counts \0
	
	return 69; //return a distinct number (for ease of troubleshooting while coding)
}


