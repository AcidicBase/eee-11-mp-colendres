#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include<math.h>
#include "error.h"
#include "integration.h"
#include "parser.h"

int main (int argc, char *argv[])
{

		//identify which values belong to which arguments
	
		int i;
		int c = 0;
		int d = 0;
		int r = 0; //c,d,r can never have 0 as an index. Zero value will be used for error checking
		
		//go across argv array 1 by 1 to find index of -c, -r and -d
		for(i = 1; i < argc; i++) //starts at 1 because argv[0] is execute command
		{	
			if(!(strcmp(argv[i], "-c")))
			{
				if(c) error(0);
				c = i;
			}
			else if(!(strcmp(argv[i], "-r")))
			{
				if(r) error(0);
				r = i;
			}
			else if(!(strcmp(argv[i], "-d")))
			{
				if(d) error(0);
				d = i;
			}
			else if(argv[i][0] == '-' && !((!(strcmp(argv[i],"-d"))) || (!(strcmp(argv[i], "-c"))) || (!(strcmp(argv[i], "-r"))))) error(0);
		}

		check(c, r, d, argc, argv); //checking if inputs are complete

		int power = parser(c+1, strlen(argv[c+1]), argv, -1);
		if (power > 3) error(12); 
		if(!(parser(r+1,strlen(argv[r+1]),argv,-1))) error(22); //checks if -r has 2 numbers
		//coeff_num = -1 tells algo to count commas

		//parse the numbers given from strings into int/float
		// using fxn to parse coefficients from the string given and putting them into an array
		int coeff[4] = {0,0,0,0}; 
		//populate arrays with 0s first so the algorithm only populates the coefficients
		//designated by the user and leaves undesignated coefficients as 0.
		//This is so that the user can input less than four values to indicate a polynomial
		//of lower power. ex: 3,2,1 will be interpreted as 0X^3 +  3X^2 + 2X +1
		 
		long unsigned int j = 3-power;

		//j will initialize as 3-power. If there are 3 commas in the input
		//the polynomial given is to the power of 3
		//algorithm will assume that it needs to populate the array starting from index 0
		//If there are 2, only three values were given, and program will assume that 
		//the polynomial is only up to the power of 2, therefore leaving the 
		//array index corresponding to X^3 0, starting array population at
		//coeff[3-power], or coeff[1]
		//power will indicate which indeces in the array will be populated


		//coeff[0] -> coefficient of X^3
		//coeff[1] -> coefficient of X^2
		//coeff[2] -> coefficient of X^1
		//coeff[3] -> coefficient of X^0

		int a = 0; //this is to indicate what nth number to get from the input
		for(; j < sizeof(coeff)/sizeof(int); j++)
		{
			coeff[j] = parser(c+1,strlen(argv[c+1]), argv, ++a);  
		}

		//getting lower bound
		int lowbd = parser(r+1,strlen(argv[r+1]), argv, 1); //1st number

		//getting upper bound		
		int upbd = parser(r+1,strlen(argv[r+1]), argv, 2); // 2nd number
		
		if (upbd < lowbd) //if given upper bound is actually the lower bound, this will switch their var assignments
		{
			int *temp = 0;
			temp = malloc(sizeof(int));
			memcpy(temp, &upbd, sizeof(int));
			memcpy(&upbd, &lowbd, sizeof(int));
			memcpy(&lowbd, temp, sizeof(int));
			free(temp);
		}	

		//getting delta
		double delta;
		if (d)
		{
			delta = atof(argv[d+1]);
		}
		else delta = 0.01;

		double ap_answer = integ(coeff,sizeof(coeff)/sizeof(int), lowbd,upbd,delta);
		double cl_answer = short_integ(coeff, sizeof(coeff)/sizeof(int),lowbd,upbd);
		
		printf("Equation: %dX^3 + %dX^2 + %dX + %d\n",coeff[0],coeff[1],coeff[2],coeff[3]);
		printf("Range: %d to %d\n", lowbd, upbd);
		printf("Delta: %lf\n", delta);
		printf("Approximated answer: %lf\n", ap_answer);
		printf("Answer using closed solution: %lf\n", cl_answer);
		printf("%%Error: %lf%%\n", ((fabs(ap_answer-cl_answer))/cl_answer)*100);
	
	return 0;
}
